<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::middleware(['auth', 'verified'])->group(function () {

	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/perfil', 'PerfilController@index')->name('perfil');
	Route::resource('publicaciones', 'PublicacionesController');
	Route::resource('comentarios', 'ComentarioController');
	//Route::get('publicaciones.editar', function () {   return view('publicaciones.editar');});
	//Route::get('/publicacion/editar')->name('publicacion/editar');


});

Route::get('/', function () {
    return view('welcome');
});

