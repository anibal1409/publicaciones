<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publicacion extends Model
{
    //
    protected $table = 'publicaciones';

	protected $fillable = [
	        'titulo', 'descripcion', 'id_user',
	];

    public function usuario()
    {
    	return $this->belongsTo("App\User","id_user");
    }

    public function comentario()
    {
    	return $this->HasMany("App\Comentario", "id_publicaciones");
    }
}
