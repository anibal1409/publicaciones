<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Publicacion;

class InicioContoller extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $publicacion = Publicacion::orderBy('created_at', 'desc')->get();

        return view('perfil.incio')->with('publicacion', $publicacion);

    }
}
