<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Publicacion;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //$publicacion = Publicacion::orderBy('created_at', 'desc')->get();
        $publicacion = Publicacion::with('comentario')->orderBy('created_at', 'desc')->get();
        

        return view('home')->with('publicacion', $publicacion);

    }
}
