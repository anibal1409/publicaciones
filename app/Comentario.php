<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    //
    protected $table = 'comentarios';

	protected $fillable = [
	        'comentario', 'id_user', 'id_publicaciones',
	];

    public function publicacion()
    {
    	return $this->belongsTo("App\Publicacion", "id_publicaciones");
    }

    public function usuario()
    {
    	return $this->belongsTo("App\User", "id_user");
    }
}
