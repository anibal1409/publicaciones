<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Persona;
use App\Contacto;
use App\Direccion;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user= User::create([
            'username' => 'admin',
            'email'=>'montilla.angela93@gmail.com',
            'password' => bcrypt(1234),
            'estatus' => 1,
            'remember_token' => str_random(10),
            'email_verified_at' => date('Y-m-d H:i:s')
        ]);

        Persona::create([
            'user_id'=> 1,
            'nombre'=> 'Angela',
            'apellido'=> 'Montilla',
            'tipo_persona'=> 'V',
            'cedula'=> '23533432',
            'fecha_nacimiento'=> '1993/07/25',
        ]);

        Contacto::create([
            'persona_id'=> 1,
            'tipo'=> 0,
            'valor'=> '04120862361',
            'predeterminado'=> 1,
            ]);

            Contacto::create([
            'persona_id'=> '1',
            'tipo'=> '1',
            'valor'=> 'montilla.angela93@gmail.com',
            'predeterminado' => 1
        ]);

        Direccion::create([
            'persona_id'=> 1,
            'calle'=> 'Av. los Cortijos',
            'casa'=> 'Bucare piso 3',
            'num'=> 'Apto 3E',
            'sector'=> 'La Viña',
            'ciudad'=> 'Maturín',
            'estado'=> 'Monagas',
            'municipio'=> 'Maturín',
            'parroquia'=> 'Las Cocuizas',
            'predeterminado'=> 1, 
        ]); 
    }
}
