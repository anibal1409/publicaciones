<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Examen;
use App\PrecioExamen;
use Illuminate\Support\Facades\DB;

class ExamenController extends Controller
{
    //los cambios en precio no estan realizados
    public function crear(Request $request){
        $validator = Validator::make($request->all(), [
            'nombre'            => 'required|string|min:5|max:150|unique:examenes,nombre',
            'descripcion'       => 'nullable|string|min:10|max:255',
            'palabras_clave'    => 'nullable|string|min:10|max:255',
            'condiciones'       => 'nullable|string|min:10|max:255',
            'hora_maxima'       => array('nullable','regex:/^(?:\d|[01]\d|2[0-3]):[0-5]\d((?:|:[0-5]\d))$/'),
            'categorias'        => 'nullable|array',
            'monto'             => array('required','regex:/^\d+(\.\d{1,2})?$/'),
        ]);
        if($validator->fails()){
            return response()->json([
                'status'  =>'error',
                'message' =>$validator->errors(),
                'a'=>$request->all()
            ], 400);
        }

        try{
            DB::beginTransaction();
            $examen                 = new Examen();
            $examen->nombre         = $request->nombre;
            $examen->descripcion    = $request->descripcion;
            $examen->palabras_clave = $request->palabras_clave;
            $examen->condiciones    = $request->condiciones;
            $examen->hora_maxima    = $request->hora_maxima;
            $examen->save();
            $examen->categorias()->sync($request->categorias);

            $precio_examen = new PrecioExamen();
            $precio_examen->id_examen = $examen->id;
            $precio_examen->monto = $request->monto;
            $precio_examen->examen()->associate($examen);
            $precio_examen->save();
            DB::commit();

            return response()->json([
                'status'    => 'success',
                'message'   => 'Se ha creado el examen con exito',
                'id'        => $examen->id
            ],200);

        } catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'status'    => 'error',
                'message'   => $e->getMessage(),
            ]);
        }
    }
    public function modificar(Request $request){
        if(!$request->id){
            return response()->json([
                'status'    => 'error',
                'message'   => "el campo id es requerido",
            ],400);
        }
        $validator = Validator::make($request->all(), [
            'nombre'            => 'required|string|min:5|max:150|unique:examenes,nombre,'.$request->id,
            'descripcion'       => 'nullable|string|min:10|max:255',
            'palabras_clave'    => 'nullable|string|min:10|max:255',
            'condiciones'       => 'nullable|string|min:10|max:255',
            'hora_maxima'       => array('nullable','regex:/^(?:\d|[01]\d|2[0-3]):[0-5]\d((?:|:[0-5]\d))$/'),
            'monto'             => array('required','regex:/^\d+(\.\d{1,2})?$/'),
        ]);
        if($validator->fails()){
            return response()->json([
                'status'  =>'error',
                'message' =>$validator->errors()
            ], 400);
        }
        try{
            DB::beginTransaction();
            $examen                 = Examen::findOrFail($request->id);
            $examen->nombre         = $request->nombre;
            $examen->descripcion    = $request->descripcion;
            $examen->palabras_clave = $request->palabras_clave;
            $examen->condiciones    = $request->condiciones;
            $examen->hora_maxima    = $request->hora_maxima;
            $examen->save();
            $examen->categorias()->sync($request->categorias);

            $precio_examen = new PrecioExamen();
            $precio_examen->id_examen = $examen->id;
            $precio_examen->monto = $request->monto;
            $precio_examen->examen()->associate($examen);
            $precio_examen->save();
            DB::commit();

            return response()->json([
                'status'    => 'success',
                'message'   => 'Se ha actualizado el examen con exito',
            ],200);

        } catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'status'    => 'error',
                'message'   => $e->getMessage(),
            ]);
        }
    }
    public function eliminar(Request $request){
        $validator = Validator::make($request->all(), [
            'id'  => 'required',
        ]);
        if($validator->fails()){
            return response()->json([
                'status'  =>'error',
                'message' =>$validator->errors()
            ], 400);
        }

        try{
            DB::beginTransaction();
            $examen  = Examen::findOrFail($request->id);
            $examen->categorias()->sync([]);
            $examen->delete();
            PrecioExamen::where('id_examen',$request->id)->delete();
            DB::commit();

            return response()->json([
                'status'    => 'success',
                'message'   => 'Se ha eliminado el examen con exito',
            ],200);

        } catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'status'    => 'error',
                'message'   => $e->getMessage(),
            ]);
        }
    }
    public function cambiarEstado(Request $request){
        $validator = Validator::make($request->all(), [
            'id'          => 'required',
            'habilitado'  => 'required|boolean',
        ]);
        if($validator->fails()){
            return response()->json([
                'status'  =>'error',
                'message' =>$validator->errors()
            ], 400);
        }
        $examen = Examen::findOrFail($request->id);
        $examen->habilitado = $request->habilitado;
        $examen->save();

        return response()->json([
            'status'    => 'success',
            'message'   => "Se ha cambiado el estado del examen con exito",
        ],200);
    }


}
