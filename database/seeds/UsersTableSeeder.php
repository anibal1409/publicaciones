<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'usuario' => 'admin',
            'name' => 'Anibal',
            'email'=>'anibal-1409@hotmail.com',
            'password' => bcrypt(1234),
            'remember_token' => str_random(10),
            'email_verified_at' => date('Y-m-d H:i:s'),
            'fecha' => '1995-09-14'
        ]);
        User::create([
            'usuario' => 'admin2',
            'name' => 'Anibal Antonio',
            'email'=>'anibal-1419@hotmail.com',
            'password' => bcrypt(1234),
            'remember_token' => str_random(10),
            'email_verified_at' => date('Y-m-d H:i:s'),
            'fecha' => '1995-09-14'
        ]);

    }
}
