@extends('layouts.master')

@section('cuerpo')
<div class="card" id="bodyPublicacion" >
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="header ">
                <h2>
                    Editar publicación
                </h2>                       
            </div>
            <div class="body" >
                <form method="POST" action="{{ url('publicaciones/'.$publicacion->id.'') }}">
                    @csrf
                    @method('PUT')
                    <label for="titulo">Título</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" id="titulo" class="form-control" name="titulo" placeholder="Ingrese el título de la publicación" required="" value="{{$publicacion->titulo}}">
                        </div>
                    </div>
                    <label for="descripcion">Descripción</label>
                    <div class="form-group">
                        <div class="form-line">
                            <textarea id="descripcion" name="descripcion" cols="30" rows="2" class="form-control no-resize" required="" aria-required="true" aria-invalid="false">{{$publicacion->descripcion}}</textarea>
                        </div>
                    </div>
                    <div style="display: inline;">
                        <input type="submit" name="enviar" class="btn btn-primary m-t-15 waves-effect" value="Enviar">
                        <button class="btn btn-danger m-t-15 waves-effect" onclick="{{ url('home') }}">Cancelar</button>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
