
@extends('layouts.master')

@section('cuerpo')
<div class="container">
    <div class="media">
        <div class="media-left">
        </div> 
        <div class="media-body">
        </div> 
        <div class="media-right">
            <button id="boton+" class="btn btn-primary btn-circle waves-effect waves-circle waves-float" onclick="mostrarPublicar()">
               <i class="material-icons" >add</i>
           </button>
           <button id="boton-" class="btn bg-pink btn-circle waves-effect waves-circle waves-float" onclick="ocultarPublicar()" style="display: none;">
            <i class="material-icons">remove</i>
        </button>
    </div>   
</div>                     
<div class="card" id="bodyPublicacion" style="display: none;">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="header ">
                <h2>
                    Nueva
                </h2>                       
            </div>
            <div class="body" >
                <form method="POST" action="{{ url('publicaciones') }}">
                    @csrf
                    <label for="titulo">Título</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" id="titulo" class="form-control" name="titulo" placeholder="Ingrese el título de la publicación" required="">
                        </div>
                    </div>
                    <label for="descripcion">Descripción</label>
                    <div class="form-group">
                        <div class="form-line">
                            <textarea id="descripcion" name="descripcion" cols="30" rows="2" class="form-control no-resize" required="" aria-required="true" aria-invalid="false"></textarea>
                        </div>
                    </div>
                    <input type="submit" name="enviar" class="btn btn-primary m-t-15 waves-effect" value="Publicar">
                </form>
            </div>
        </div>
    </div>
</div>
<div>
    @foreach ($publicacion as $registro) 
    <div class="panel panel-default panel-post">
        <div class="panel-heading">
            <div class="media">
                <div class="media-left">
                    <a>
                        <img src="{{ asset('recursos/img/user.png') }}" />
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">
                        {{$registro->usuario->name}}
                    </h4>
                    Creado:  {{date_format($registro->created_at, 'd/m/Y g:i A') }}
                </div>
                <div class="media-right">
                    <div style="display: inline-flex;">
                        <a href="{{ url('publicaciones/'.$registro->id.'/edit') }}">
                            <button type="submit" class="btn bg-green btn-circle waves-effect waves-circle waves-float">
                                <i class="material-icons">create</i>
                            </button>
                        </a>
                        <form method="POST" action="{{ url('publicaciones/'.$registro->id.'') }}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn bg-pink btn-circle waves-effect waves-circle waves-float">
                                <i class="material-icons">clear</i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="post">
                <div class="post-heading">
                    <strong>{{$registro->titulo}}</strong>
                    <p>{{$registro->descripcion}}</p>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <ul>
                <li>
                    <a href="#">
                        <i class="material-icons">thumb_up</i>
                        <span>12 Likes</span>
                    </a>
                </li>
                <li>
                    <a onclick="mostrarComentarios({{ $registro->id }});">
                        <i class="material-icons" >comment</i>
                        <span>{{/*count($registro->comentario)*/ $registro->comentario->count()}} 
                            Commentarios
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="material-icons">share</i>
                        <span>Share</span>
                    </a>
                </li>
            </ul>
            <div class="form-group" id="comentarios{{ $registro->id }}" style="display: none;">
                
                @foreach ($registro->comentario as $comentario) 
                <div class="form-group">
                    <strong>
                        {{$comentario->usuario->name}}
                    </strong>
                    {{$comentario->comentario}}
                </div>
                @endforeach
                <button id="botonComentario{{ $registro->id }}" class="btn btn-primary m-t-15 waves-effect" onclick="mostrar({{ $registro->id }})">
                    Agregar
                </button>
            </div>

            
            <div class="form-group" id="contenedor{{ $registro->id }}" style="display: none;">
                <form method="POST" action="{{ url('comentarios') }}">
                    @csrf
                    <div class="form-line">
                        <input type="text" class="form-control" placeholder="Ingrese su comentario" name="comentario" />
                    </div>
                    <input type="submit" name="enviar" class="btn btn-primary m-t-15 waves-effect" value="Comentar">
                    <input type="hidden" name="publicacion" value="{{ $registro->id }}">
                </form>
            </div>
        </div>
    </div>          
    @endforeach
</div>
</div>
@endsection
<script type="text/javascript">
    function mostrar($id){
       /* document.getElementById('contenedor'+$id).innerHTML = `<div class="form-line">
                                                        <input type="text" class="form-control" placeholder="Type a comment" />
                                                        </div>`;*/

        //document.getElementById('contenedor'+$id).style.display = 'block';
        //document.getElementById('botonComentario'+$id).style.display = 'none';
        document.getElementById('contenedor'+$id).style.display = 'block';
        document.getElementById('botonComentario'+$id).style.display = 'none';
    }
    function mostrarComentarios($id){
       /* document.getElementById('contenedor'+$id).innerHTML = `<div class="form-line">
                                                        <input type="text" class="form-control" placeholder="Type a comment" />
                                                        </div>`;*/
                                                        if(document.getElementById('comentarios'+$id).style.display == 'block'){
                                                            document.getElementById('comentarios'+$id).style.display = 'none';
document.getElementById('contenedor'+$id).style.display = 'none';
document.getElementById('botonComentario'+$id).style.display = 'block';
}
else{
    document.getElementById('comentarios'+$id).style.display = 'block';
}

}
function mostrarPublicar(){
    document.getElementById('bodyPublicacion').style.display = 'block';
    document.getElementById('boton-').style.display = 'block';
    document.getElementById('boton+').style.display = 'none';
}
function ocultarPublicar(){
    document.getElementById('bodyPublicacion').style.display = 'none';
    document.getElementById('boton-').style.display = 'none';
    document.getElementById('boton+').style.display = 'block';
}
</script>

