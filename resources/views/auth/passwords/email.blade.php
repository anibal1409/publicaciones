<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Olvide mi Contraseña | Facebook</title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <!--link href="../../plugins/bootstrap/css/bootstrap.css" rel="stylesheet"-->

    <!-- Waves Effect Css -->
    <link href="{{ asset('plugins/node-waves/waves.css') }}" rel="stylesheet">
    <!--link href="../../plugins/node-waves/waves.css" rel="stylesheet" /-->

    <!-- Animation Css -->
    <link href="{{ asset('plugins/animate-css/animate.css') }}" rel="stylesheet">
    <!--link href="../../plugins/animate-css/animate.css" rel="stylesheet" /-->

    <!-- Custom Css -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <!--link href="../../css/style.css" rel="stylesheet"-->
</head>

<body class="fp-page">
    <div class="fp-box">
        <div class="logo">
            <a href="javascript:void(0);">Face<b>book</b></a>
            <small>Publicaciones y comenterios</small>
        </div>
        <div class="card">
            <div class="body">
                 <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                    <div class="msg">
                        Ingrese su dirección de correo electrónico que utilizó para registrarse. Le enviaremos un correo electrónico con su nombre de usuario y un enlace para restablecer su contraseña.
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email"placeholder="Email" id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div>

                    <button type="submit" class="btn btn-block btn-lg bg-pink waves-effect">
                        {{ __('Restaurar contraseña') }}
                    </button>


                    <div class="row m-t-20 m-b--5 align-center">
                        <a href="{{ route('login') }}">Iniciar Sesión</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!--script src="../../plugins/jquery/jquery.min.js"></script -->

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>
    <!--script src="../../plugins/bootstrap/js/bootstrap.js"></script-->

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('plugins/node-waves/waves.js') }}"></script>
    <!--script src="../../plugins/node-waves/waves.js"></script-->

    <!-- Validation Plugin Js -->
    <script src="{{ asset('plugins/jquery-validation/jquery.validate.js') }}"></script>
    <!--script src="../../plugins/jquery-validation/jquery.validate.js"></script-->

    <!-- Custom Js -->
    <script src="{{ asset('js/admin.js') }}"></script>
    <!--script src="../../js/admin.js"></script-->
    <script src="{{ asset('js/pages/examples/forgot-password.js') }}"></script>
    <!--script src="../../js/pages/examples/forgot-password.js"></script-->
</body>

</html>